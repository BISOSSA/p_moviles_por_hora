CREATE OR REPLACE PROCEDURE p_moviles_por_hora AS
  CURSOR c_moviles IS
    SELECT nume_base
          ,nomb_esta_movi AS estadomovil
          ,m.nume_movi AS moviles
          ,CASE
             WHEN m.nume_tipo_movi IN (2, 1) THEN
              'TRASLADO'
             WHEN m.nume_tipo_movi IN (9, 3) THEN
              'MECANICO'
             WHEN m.nume_tipo_movi = 6 THEN
              'DUAL'
           END tipo_movil
          ,CASE
             WHEN m.nume_tipo_cont = 1 THEN
              'PRIVATIZADO SIN MANTENIMIENTO'
             WHEN m.nume_tipo_cont = 2 THEN
              'CONTRATADO FIJO'
             WHEN m.nume_tipo_cont = 3 THEN
              'EVENTUAL'
             WHEN m.nume_tipo_cont = 4 THEN
              'HIBRIDO'
             WHEN m.nume_tipo_cont = 5 THEN
              'PRIVATIZADO C/MANT PROPIO'
           END tipo_contrato
      FROM moviles m
     INNER JOIN (SELECT nume_movi
                       ,nume_base
                       ,nume_inte
                       ,bm.nume_esta_movi
                       ,nomb_esta_movi
                   FROM basesmoviles bm
                  INNER JOIN estadosmoviles em
                     ON bm.nume_esta_movi = em.nume_esta_movi) bm
        ON m.nume_movi = bm.nume_movi
     INNER JOIN (SELECT nume_movi
                       ,nomb_pres
                   FROM basesmoviles bm
                  INNER JOIN (SELECT nume_base
                                   ,nomb_pres
                               FROM bases b
                              INNER JOIN prestadores p
                                 ON b.nume_pres = p.nume_pres) bp
                     ON bm.nume_base = bp.nume_base
                  WHERE base_defa = 1) bmp
        ON m.nume_movi = bmp.nume_movi
     WHERE nume_base IN (6001, 10541, 5052, 8700)
       AND bm.nume_esta_movi NOT IN (0, 3)
       AND m.nume_esta = 1
     GROUP BY nume_base
             ,nomb_esta_movi
             ,m.nume_movi
             ,m.nume_tipo_movi
             ,m.nume_tipo_cont
     ORDER BY nume_base;
  v_hora    VARCHAR2(2);
  v_fecha   DATE;
  v_count   NUMBER(2);
  v_cant    NUMBER(2);
  v_minutos VARCHAR(30);
  v_min     NUMBER(2);
BEGIN
  SELECT to_char(SYSDATE, 'hh24')
        ,trunc(SYSDATE)
        ,CASE
           WHEN to_char(SYSDATE, 'mi') BETWEEN 0 AND 15 THEN
            'Primer cuarto de hora'
           WHEN to_char(SYSDATE, 'mi') BETWEEN 16 AND 30 THEN
            'Segundo cuarto de hora'
           WHEN to_char(SYSDATE, 'mi') BETWEEN 31 AND 45 THEN
            'Tercer cuarto de hora'
           ELSE
            'Cuarto cuarto de hora'
         END
    INTO v_hora
        ,v_fecha
        ,v_minutos
    FROM dual;

  FOR r IN c_moviles LOOP
  
    SELECT COUNT(1)
      INTO v_count
      FROM moviles_por_hora
     WHERE nume_movi = r.moviles
       AND hora = v_hora;
  
    SELECT COUNT(1)
      INTO v_cant
      FROM moviles_por_hora
     WHERE nume_movi = r.moviles
       AND fecha = v_fecha
       AND hora = v_hora;
  
    SELECT COUNT(1)
      INTO v_min
      FROM moviles_por_hora
     WHERE nume_movi = r.moviles
       AND fecha = v_fecha
       AND hora = v_hora
       AND cuarto_de_hora = v_minutos;
  
    IF (v_count = 0 AND v_cant = 0 AND v_min = 0) OR (v_count = 0 AND v_cant >= 1 AND v_min = 0) OR (v_count >= 1 AND v_cant = 0 AND v_min = 0) OR
       (v_count >= 1 AND v_cant >= 1 AND v_min = 0) OR (v_count = 0 AND v_cant >= 1 AND v_min >= 1) OR (v_count >= 1 AND v_cant = 0 AND v_min >= 1) THEN
      /*(v_count = 0 AND v_cant = 0) OR (v_count = 0 AND v_cant > 1) OR (v_count = 1 AND v_cant = 0) THEN
      IF v_min = 0  OR v_count = 0 OR v_cant = 0 THEN*/
      INSERT INTO moviles_por_hora
        (nume_base
        ,estadomovil
        ,nume_movi
        ,hora
        ,fecha
        ,tipo_movil
        ,cuarto_de_hora
        ,tipo_contrato)
      VALUES
        (r.nume_base
        ,r.estadomovil
        ,r.moviles
        ,v_hora
        ,to_date(to_char(v_fecha, 'dd/mm/yyyy'), 'dd/mm/yyyy')
        ,r.tipo_movil
        ,v_minutos
        ,r.tipo_contrato);
      COMMIT;
    END IF;
  END LOOP;
  COMMIT;
END p_moviles_por_hora;
/
